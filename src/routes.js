import React from 'react';

const Home = React.lazy(() => import("./pages/Home"));
const Characters = React.lazy(() => import("./pages/Characters"));
const Comics = React.lazy(() => import("./pages/Comics"));
const Stories = React.lazy(() => import("./pages/Stories"));

const routes = [
    { path: "/", exact: true, name: "home", component: Home },
    { path: "/characters/:id", exact: true, name: "characters", component: Characters },
    { path: "/comics/:id", exact: true, name: "comics", component: Comics },
    { path: "/stories/:id", exact: true, name: "stories", component: Stories }
];

export default routes;

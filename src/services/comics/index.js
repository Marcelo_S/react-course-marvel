import axios from "axios"

export const getAllComics = async (options = {}) => {
    const {
        format = "",
        title = "",
        issue = "",
        limit = 20,
        offset = 0,
        order = "title",
        characterId = 0,
        storyId = 0,
    } = options

    let params = {
        apikey: process.env.REACT_APP_PUBLIC_KEY,
        orderBy: order,
        limit,
        offset,
    }

    let url = `${process.env.REACT_APP_API_URL}/comics`

    if (format) {
        params.format = format
    }

    if (title) {
        params.titleStartsWith = title
    }

    if (issue) {
        params.issueNumber = issue
    }

    if (characterId) {
        url = `${process.env.REACT_APP_API_URL}/characters/${characterId}/comics`
    }

    if (storyId) {
        url = `${process.env.REACT_APP_API_URL}/stories/${storyId}/comics`
    }

    try {
        const fetched = await axios.get(url, {
            params,
        })

        return { data: fetched.data.data }
    } catch (error) {
        return { error }
    }
}

export const getComic = async id => {
    if (!id) {
        return { error: { status: "Comic id is required" } }
    }

    try {
        const fetched = await axios.get(
            `${process.env.REACT_APP_API_URL}/comics/${id}`,
            {
                params: {
                    apikey: process.env.REACT_APP_PUBLIC_KEY,
                },
            }
        )

        return { data: fetched.data.data }
    } catch (error) {
        return { error }
    }
}

import axios from "axios"

export const getAllStories = async (options = {}) => {
    const { limit = 20, offset = 0, characterId = 0, comicId = 0 } = options

    let params = {
        apikey: process.env.REACT_APP_PUBLIC_KEY,
        limit,
        offset,
    }

    let url = `${process.env.REACT_APP_API_URL}/stories`

    if (characterId) {
        url = `${process.env.REACT_APP_API_URL}/characters/${characterId}/stories`
    }

    if (comicId) {
        url = `${process.env.REACT_APP_API_URL}/comics/${comicId}/comics`
    }

    try {
        const fetched = await axios.get(url, {
            params,
        })

        return { data: fetched.data.data }
    } catch (error) {
        return { error }
    }
}

export const getStory = async id => {
    if (!id) {
        return { error: { status: "Story id is required" } }
    }

    try {
        const fetched = await axios.get(
            `${process.env.REACT_APP_API_URL}/stories/${id}`,
            {
                params: {
                    apikey: process.env.REACT_APP_PUBLIC_KEY,
                },
            }
        )

        return { data: fetched.data.data }
    } catch (error) {
        return { error }
    }
}

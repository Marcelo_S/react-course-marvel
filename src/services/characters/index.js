import axios from 'axios';

export const getAllCharacters = async (options = {}) => {
    const {
        name = "",
        series = "",
        limit = 20,
        offset = 0,
        comicId = 0,
    } = options;

    let params = {
        apikey: process.env.REACT_APP_PUBLIC_KEY,
        limit, offset
    }

    if (name) {
        params.nameStartsWith = name;
    }

    if (series) {
        params.series = series;
    }

    let url = `${process.env.REACT_APP_API_URL}/characters`;

    if (comicId) {
        url = `${process.env.REACT_APP_API_URL}/comics/${comicId}/characters`;
    }

    try {
        const fetched = await axios.get(url, { params });
        console.log('fetched', fetched);
        return { data: fetched.data.data }
    } catch (error) {
        return { error }
    }
}

export const getCharacterById = async (id) => {
    if (!id) {
        return { error: { status: "Id is required" } };
    }

    try {
        const fetched = await axios.get(`${process.env.REACT_APP_API_URL}/characters/${id}`, {
            params: {
                apikey: process.env.REACT_APP_PUBLIC_KEY,
            }
        });
        console.log('fetched.data.data', fetched.data.data);
        return { data: fetched.data.data };
    } catch (error) {
        console.log('error', error);
        return { error }
    }
}

export const getCharacterComics = async (id) => {
    if (!id) {
        return { error: { status: "Id is required" } };
    }

    try {
        const fetched = await axios.get(`${process.env.REACT_APP_API_URL}/characters/${id}/comics`, {
            params: {
                apikey: process.env.REACT_APP_PUBLIC_KEY
            }
        });

        console.log('character comics', fetched.data.data);
        return { data: fetched.data.data };
    } catch (error) {
        console.log('error', error);
        return { error };
    }
}

export const getCharacterStories = async (id) => {
    if (!id) {
        return { error: { status: "Id is required" } };
    }

    try {
        const fetched = await axios.get(`${process.env.REACT_APP_API_URL}/characters/${id}/stories`, {
            params: {
                apikey: process.env.REACT_APP_PUBLIC_KEY
            }
        });

        console.log('data', fetched.data.data);
        return { data: fetched.data.data }
    } catch (error) {
        console.log('error', error);
        return { error }
    }
}
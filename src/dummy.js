import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ACTION_ADD_FAVORITE_COMICS } from "./redux/constants";

export default function Dummy() {
    const comics = useSelector(state => state.favorites.comics);
    const dispatch = useDispatch();

    const addNewFavorite = () => {
        dispatch({ type: ACTION_ADD_FAVORITE_COMICS, payload: { id: 2, name: "Avengers" } })
    }

    useEffect(() => {
        console.log('comics', comics);
    }, [comics])

    return (
        <div>
            <p>check console</p>
            <button onClick={addNewFavorite}>Agregar fav</button>
        </div>
    )
}

import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from "redux-persist/integration/react";
import { getStore, getPersistor } from "./redux/store";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import routes from "./routes";

import "./assets/scss/main.scss";

const loading = () => <div className="">Loading...</div>;

function App() {
  const store = getStore();
  const persistor = getPersistor(store);

  const renderRoutes = () => {
    return routes.map((route, idx) => {
      return route.component ? (
        <Route
          key={idx}
          path={route.path}
          exact={route.exact}
          name={route.name}
          render={(props) => <route.component {...props} />}
        />
      ) : null;
    })
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <div className="App">
          <BrowserRouter>
            <React.Suspense fallback={loading()}>
              <Switch>
                {renderRoutes()}
              </Switch>
            </React.Suspense>
          </BrowserRouter>
        </div>
      </PersistGate>
    </Provider>
  );
}

export default App;

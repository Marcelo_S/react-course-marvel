import React from 'react';
import MainLogo from "../../assets/images/logos/marvel-logo.png";

export default function index() {
    return (
        <nav className="Navbar">
            <a href="https://www.marvel.com/" className="Navbar-brand">
                <img src={MainLogo} alt="Marvel Logo" className="Navbar-logo" />
            </a>
            <ul className="Navbar-items">
                <li className="Navbar-item">
                    <a className="Navbar-link" to="/">Home</a>
                </li>
                <li className="Navbar-item">
                    <a className="Navbar-link" to="/">Comics</a>
                </li>
                <li className="Navbar-item">
                    <a className="Navbar-link" to="/">Characters</a>
                </li>
                <li className="Navbar-item">
                    <a className="Navbar-link" to="/">Stories</a>
                </li>
                <li className="Navbar-item">
                    <a className="Navbar-link" to="/">Favorites</a>
                </li>
            </ul>
        </nav>
    );
}

import React from 'react';
import MainLogo from "../../assets/images/logos/marvel-logo.png";

export default function Footer() {
    return (
        <footer className="Footer">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-6">
                        <img src={MainLogo} alt="Marvel Comics" className="Footer-logo" />
                    </div>
                    <div className="col-12 col-md-6">
                        <ul className="Footer-social">
                            <li>
                                <a href="#" className="Footer-link">
                                    <span className="sr-only">Facebook</span>
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" className="Footer-link">
                                    <span className="sr-only">Twitter</span>
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" className="Footer-link">
                                    <span className="sr-only">Instagram</span>
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" className="Footer-link">
                                    <span className="sr-only">YouTube</span>
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" className="Footer-link">
                                    <span className="sr-only">Pinterest</span>
                                    <i class="fab fa-pinterest-p"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" className="Footer-link">
                                    <span className="sr-only">Tumblr</span>
                                    <i class="fab fa-tumblr"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" className="Footer-link">
                                    <span className="sr-only">Snapchat</span>
                                    <i class="fab fa-snapchat-ghost"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    )
}

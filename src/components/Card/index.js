import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  ACTION_ADD_FAVORITE_COMICS,
  ACTION_ADD_FAVORITE_CHARACTERS,
  ACTION_ADD_FAVORITE_STORIES,
  ACTION_REMOVE_FAVORITE_COMICS,
  ACTION_REMOVE_FAVORITE_CHARACTERS,
  ACTION_REMOVE_FAVORITE_STORIES,
} from "../../redux/constants";

export default function Card(props) {
  const isFavorite = useSelector((state) => {
    return !!state.favorites[props.type].find((f) => f.id === props.id);
  });

  const dispatch = useDispatch();

  const getFavoriteActions = () => {
    switch (props.type) {
      case "characters":
        return {
          add: ACTION_ADD_FAVORITE_CHARACTERS,
          remove: ACTION_REMOVE_FAVORITE_CHARACTERS,
        };
      case "comics":
        return {
          add: ACTION_ADD_FAVORITE_COMICS,
          remove: ACTION_REMOVE_FAVORITE_COMICS,
        };
      case "stories":
        return {
          add: ACTION_ADD_FAVORITE_STORIES,
          remove: ACTION_REMOVE_FAVORITE_STORIES,
        };

      default:
        break;
    }
  };

  const toggleFavorite = () => {
    const { id, name, type } = props;
    const action = getFavoriteActions();
    if (isFavorite) {
      dispatch({ type: action.remove, payload: props.id });
    } else {
      dispatch({ type: action.add, payload: { id, name, type } });
    }
  };

  return (
    <div className="Card">
      <button className="Card-favorite" onClick={toggleFavorite}>
        <span className="sr-only">Add to favorites</span>
        <i class={isFavorite ? "fas fa-star" : "far fa-star"}></i>
      </button>
      <a href="/" className="Card-link">
        <div className="Card-body">
          <img className="Card-image" src={props.imageUrl} alt="Lobezno" />
          <div className="Card-title">
            <h3 className="Card-name">{props.name || ""}</h3>
          </div>
        </div>
      </a>
    </div>
  );
}

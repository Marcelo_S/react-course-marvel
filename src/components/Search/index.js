import React from 'react'

export default function Search() {
    return (
        <div className="Search">
            <div className="Search-controls">
                <button className="Search-control">All</button>
                <button className="Search-control">Characters</button>
                <button className="Search-control">Comics</button>
            </div>
            <input type="text" placeholder="Search" className="Search-input" />
        </div>
    )
}

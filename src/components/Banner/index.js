import React from 'react'

export default function Banner(props) {
    return (
        <section className="Banner">
            <div className="Banner-content">
                {props.children}
            </div>
        </section>
    )
}

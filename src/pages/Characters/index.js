import React, { Fragment, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { getCharacterById } from "../../services/characters";

export default function Character() {
  const { id } = useParams();

  const [character, setcharacter] = useState({});

  useEffect(() => {
    fetchCharacter();
  }, []);

  const fetchCharacter = async () => {
    const {
      data: { results },
      error,
    } = await getCharacterById(id);
    console.log("data", results);

    const [data] = results;

    console.log("data", data);
    setcharacter(data);
  };

  const renderCharacter = () => {
    return (
      <div className="d-flex flex-row">
        <img
          className="image-fluid"
          src={`${character.thumbnail?.path}.${character.thumbnail?.extension}`}
          alt=""
        />
        <div className="ml-4">
          <h3 className="d-block mb-3">{character.name}</h3>
          <span>{character.description} </span>
        </div>
      </div>
    );
  };

  return <Fragment> {renderCharacter()} </Fragment>;
}

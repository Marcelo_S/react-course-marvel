import React from 'react';
import { useParams } from 'react-router-dom';

export default function Comic() {
    const { id } = useParams();

    return (
        <div>
            <h1>Comic {`${id}`}</h1>
        </div>
    )
}

import React, { useState, useEffect } from "react";
import Banner from "../../components/Banner";
import Navbar from "../../components/Navbar";
import Search from "../../components/Search";
import Card from "../../components/Card";
import Footer from "../../components/Footer";
import { Link } from "react-router-dom";
import { getAllCharacters } from "../../services/characters";

export default function Index() {
  const [characters, setcharacters] = useState([]);

  useEffect(() => {
    get();
  }, []);

  const get = async () => {
    const {
      data: { results },
      error,
    } = await getAllCharacters();

    setcharacters(results);
  };

  const renderCharacter = () => {
    return (
      characters &&
      characters.map((item) => {
        return (
          <Link to={`/characters/${item.id}`}>
            <Card
              type="characters"
              id={item.id}
              name={item.name}
              imageUrl={`${item.thumbnail.path}.${item.thumbnail.extension}`}
            />
          </Link>
        );
      })
    );
  };

  return (
    <div className="Home">
      <Banner>
        <Navbar />
        <Search />
      </Banner>
      <section className="Featured">
        <h2 className="Featured-title">Featured Characters</h2>
        <div className="Featured-content">{renderCharacter()}</div>
      </section>
      <Footer />
    </div>
  );
}
